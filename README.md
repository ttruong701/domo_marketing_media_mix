# Marketing Media Mix Planning App

Similiar to the CCCJ app, I will be creating a data viz application that outlines
the relative budget spends for each marketing media through the customer journey stages.

# Table of Contents
* [Getting Started](#getting-started)
    * [Prequisites](#prerequisites)
    * [Installing](#installing)
* [Testing](#testing)
* [Deployment](#deployment)
* [Built With](#built-with)
* [Contributing](#contributing)
* [Versioning](#versioning)
* [Authors](#authors)
* [License](#license)
* [Acknowledgements](#acknowledgements)

## Getting Started
These instructiosn will get you a copy of the project running locally for development.
See the deployment section to learn how to deploy this project live.

### Prerequisites
* Git Bash for Windows or terminal

### Installing
Clone the repository:

```
git clone git@bitbucket.org:rdi-domo/domo-local-seo-app.git
```

This will pull down other directories, so navigate to the right directory:

```
cd marketing_media_mix
```

In this directory, run:

```
npm install
```

After the Node and Gulp dependencies are installed, deploy the local server:

```
gulp
```

Gulp will watch the *.html, *.css and *.js files for changes, and refresh the
local server to display the changes. Using gulp will load the visualizations 
with sample data. To use actual Domo data from connectors, see the
[Deployment](#deployment) section.

## Testing
TBA

## Deployment
On Git Bash or terminal in this directory, run:

```
domo login
```

Select the RDI production instance and login. Afterwards, run:


```
domo publish
```

The Domo application will now be published within the RDI instance, where it can
be used to create new cards.

To locally develop the app through the Domo local environment, run:

```
domo dev
```

This will start the Domo local server and serve your files. If the application
is published, the server will allow you to query data from Domo DataSets!

## Built With
* D3.js V4
* Gulp
* Domo.js
* Pure.css

## Contributing
TBD

## Versioning
See the CHANGELOG.md. WE use SemVer for versioning.

## Authors
* Tommy Truong, Marketing Technology Intern, Summer '17 - initial work

## License
Include the MIT License

## Acknolwedgements
* Luis Retana, Marketing Technology Engineer
* Ron Hadler, Marketing Technology Director
* John Faris, VP Cross Channel Marketing