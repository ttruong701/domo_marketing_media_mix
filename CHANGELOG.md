# Changelog
by Tommy Truong, Marketing Technology Intern, Summer '17

All notable changes to this project will be documented in this file.

The format is based on [Keep a Changelog](http://keepachangelog.com/en/1.0.0/)
and this project adheres to [Semantic Versioning](http://semver.org/spec/v2.0.0.html).

## [Unreleased]
- Toggle text visibility with circle visbility
- Budget slider with interpolation
- Market maturity budget
- SASS stylesheets

## 0.0.2 - 2017-08-16
### Added
- Toggle circle visibility

## 0.0.1 - 2017-08-15
### Added
- README.md and CHANGELOG.md