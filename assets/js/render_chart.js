// load data, callback to continue
var chart,
    nodes,
    medium_selection,
    stage_selection,
    legend_selection,
    legend_items,
    legend_item_wrapper,
    width = 269,
    // height = 236
    height = 269

// Used to class the marketing tactic circles
// var media_names = /^(paid|seo|social|oim|content)$/
var media_names = ["paid", "seo", "social", "oim", "content"]
var media_colors = {
    "paid": "#5B6CA4",
    "social": "#DB7D26",
    "content": "#89AC36",
    "oim": "#C71547",
    "seo": "#14B47C"
}

// TODO Can we make this dollar format?
var format = d3.format(",d")

var pack = d3.pack()
    // .size([height - 100, width - 100])
    .size([height, width])

d3.json("data/data_old.json", function (err, data) {
    if (err) throw err
    
    // Draw funnel row by row
    // Paid, earned, owned
    data.media.forEach(function (medium) {
        // Draw each stage through a row
        // Awareness, consideration, conversion, loyalty/advocacy
        medium_selection = d3.select(".funnel")
            .append("div")
            .attr("id", medium.name)
            .attr("class", "media pure-g")

        // Iterate through stages
        medium.stages.forEach(function (stage) {
            stage_selection = medium_selection
                .append("div")
                .attr("id", stage.name)
                .attr("class", stage.name + " pure-u-1-4")
                .append("svg")
                .attr("height", "100%")
                .attr("width", "100%")
                .attr("transform", "translate(2,2)")
                .attr("viewBox", "0 0 " + width +  " " + height)
                .attr("preserveAspectRatio", "xMinYMax meet")
                .attr("class", "svg-content")
                .append("g")

            stage_data = d3.hierarchy(stage)
                .sum(function (d) { return d.size })
                .sort(function (a, b) { return b.value - a.value })

            // Pack circles
            nodes = stage_selection.selectAll(".node")
                .data(pack(stage_data).descendants()) // Assigns (x,y,r) values for all nodes
                .enter()
                .append("g") // Create packing group
                .attr("class", function (d) {
                    if (d.children) {
                        return "node"
                    } else if (media_names.includes(d.parent.data.name)) {
                        return "active leaf node " + d.parent.data.name
                    } 
                 })
                .attr("transform", function (d) { return "translate(" + d.x + "," + d.y + ")" }) // Position nodes on svg grid

            // Embed a tooltip (name\nvalue) for each node
            nodes.append("title")
                .text(function (d) { return d.data.name + "\n" + format(d.value) })

            // Draws a circle for each node
            nodes.append("circle")
                .attr("r", function (d) { return d.r })

            // Display tactic name on circle
            nodes.filter(function (d) { return !d.children })
                .append("text")
                // .attr("dy", "0.5em")
                .text(function (d) {
                    return d.data.name.substring(0, d.r / 3)

                })
            
        })
    })

    // Legend container
    legend_selection = d3.select(".legend")

    // Legend items container
    legend_items = legend_selection.selectAll("div")
                    .data(media_names)
                    .enter()
                    .append("div")
                    .attr("class", "pure-u-1-6")
                    .on("click", function(d) {
                        // Select all circles of class d
                        media_circles = d3.selectAll("." + d)
                        // Check active state
                                            .classed("active", function(d,i) {
                                                return !d3.select(this).classed("active")
                                            })
                        // Change opacity for active circles
                        // 
                    })

    legend_item_wrapper = legend_items.append("div")
                            .attr("class", "legend-item")

    legend_item_wrapper.append("span")
                        .attr("class", "legend-icon")
                        .style("background-color", function(d) {
                            return media_colors[d]
                        })

    legend_item_wrapper.append("span")
                        .attr("class", "legend-brand")
                        .text(function(d) {
                            return d
                        })


    // legend_selection = d3.select(".legend")
    //     .append("div")
    //     .attr("class", "pure-g")

    // media_names.forEach(function(d) {
    //     legend_selection
    //     .append("div")
    //     .attr("class", "pure-u-1-5")
    //     .append("svg")
    //     .append("text")
    //     // .append("x", function(d,i) {
    //     // })
    //     .text(d)
    // })

})

/*
stages: [
    {
    name: awareness
    media: [
        paid
        earned
        owned
    ]
}
]
*/