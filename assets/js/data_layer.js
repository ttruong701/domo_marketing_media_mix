/**
 * @file TODO 
 * @summary TODO
 * @author Tommy Truong <ttruong@reddoor.biz>
 * @version 0.0.1
 */

// Main functions

/**
 * Retrieves DOMO data
 * @param 
 * @returns 
 */
function getData(error, data_api, parseData) {
    if (error) console.error(error)

    var data = data_api.reduce(function(data, url) {
        domo.get(url).then(function(budget) {
            data.push(budget);
        })
        return data;
    }, []);

    if (data) {
        parseData(null, data);
    } else {
        parseData(new Error("No Data!"), data);
    }
}

/**
 * Retrieves DOMO data
 * @param 
 * @returns
 */
function parseData(error, data) {
    if (error) console.error(error)
    
    var channel_to_medium = {
        "paid media": "paid",
        "seo": "earned",
        "social": "earned",
        "oim": "owned",
        "content": "owned"
    };

    // Lookup tables for array positions
    var media = {};
    var stages = {};
    var channels = {};

    // Array positions 
    var media_index = 0;
    var stages_index = 0;
    var channels_index = 0;

    // Construct hierarchial data
    var data = data.reduce(function(trees, budget) {
        var tree = budget.reduce(function(media_tree, datum) {
            var medium,
                stage,
                channel,
                tactic;

            // 1. Use "channel" to find associated medium (string)
            // seo -> earned
            // social -> earned
            var medium_name = channel_to_medium[datum.channel]
            // FOLLOW THIS THOUGHT, do IF checks for dictionary existances and push if exists!!!
            // var medium_name = (media.hasOwnProperty(medium_name)) ? channel_to_medium[datum.channel] : 
            // var medium = (media.hasOwnProperty(medium_name)) 
            if (!(media.hasOwnProperty(medium_name))) {
                medium = makeNode(medium_name)
                media[medium_name] = media_index++;
            } else {
                // Get the correct medium node
                medium = media_tree.children[media[medium_name]]
            }

            if (stages.hasOwnProperty(datum.stage)) {
                stage = makeNode(datum.stage)
                stages[datum.stage] = stages_index++;
            } else {
                // Get the right medium node, then stage node
                stage = media_tree.children[media[]]
            }

            if (channels.hasOwnProperty(datum.channel)) {
                channel = makeNode(datum.channel)
                channels[datum.channel] = channels_index++;
            } else { channel = null }

            tactic = makeTactic(datum.tactic, datum.budget)

            if (channel) { channel.children.push(tactic) }
            if (stage) { stage.children.push(channel) }
            if (medium) { medium.children.push(stage) }

            media_tree.children.push(medium)
            
            // 2. Check if medium exists in media index dictionary
            if (media.hasOwnProperty(medium_name)) {
                // 14. Else use "stage" to find if stage index exists
                if (stages.hasOwnProperty(datum.stage)) {
                    // 16. Else use "channel" to find if channel index exists
                    if (channels.hasOwnProperty(datum.channel)) {
                        // 18. Else do step 9-10
                    } else {
                        // 17. If channel DNE, do steps 7-11
                    }
                } else {
                    // 15. If stage DNE, do steps 5-12

                    // 5. Create stage JSON object with "name": stage and "children": []
                    var stage = makeNode(datum.stage);
                    // 6. Store stage index and increment stage index counter
                    stages[datum.stage] = stages_index++;
                    // 7. Create channel JSON object with "name": channel and "children": []
                    var channel = makeNode(datum.channel);
                    // 8. Store channel index and increment chanel index ctr
                    channels[datum.channel] = channels_index++;
                    // 9. Create tactic JSON object with "name": tactic and "size": budget
                    var tactic = makeTactic(datum.tactic, datum.budget)
                    
                    // 10. Push tactic object into channel's children array
                    channel.children.push(tactic)
                    // 11. Push channel into stage's children array
                    stage.children.push(channel)
                    // 12. Push stage into medium's stages array
                    medium.stages.push(stage)


                }
            } else {
                // 3. If medium_index DNE, make the medium JSON object with "name": medium and "stages": []
                var medium = {
                    "name": medium_name,
                    "stages": []
                };
                // 4. Store medium's index and increment media index counter
                media[medium_name] = media_index++;
                // 5. Create stage JSON object with "name": stage and "children": []
                var stage = makeNode(datum.stage);
                // 6. Store stage index and increment stage index counter
                stages[datum.stage] = stages_index++;
                // 7. Create channel JSON object with "name": channel and "children": []
                var channel = makeNode(datum.channel);
                // 8. Store channel index and increment chanel index ctr
                channels[datum.channel] = channels_index++;
                // 9. Create tactic JSON object with "name": tactic and "size": budget
                var tactic = makeTactic(datum.tactic, datum.budget)

                // 10. Push tactic object into channel's children array
                channel.children.push(tactic)
                // 11. Push channel into stage's children array
                stage.children.push(channel)
                // 12. Push stage into medium's stages array
                medium.stages.push(stage)

                // 13. Push medium into tree's media array
                tree.media.push(medium)
            }

            return tree
        // }, { "media": [] });
        }, makeNode("media_tree"));
    }[]);
    return data;
}

// Helper functions
/**
 * 
 * @param
 * @returns
 */
function makeNode(name) {
    return {
        "name": name,
        "children": []
    }
}

function makeTactic(tactic, budget) {
    return {
        "name": tactic,
        "size": budget
    }
}
